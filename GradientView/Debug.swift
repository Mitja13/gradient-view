//
//  Debug.swift
//  GradientView
//
//  Created by Mitja Semolic on 02/07/2018.
//  Copyright © 2018 Mitja Semolic. All rights reserved.
//

import Foundation

struct DEBUG {
    static private var timers: [String:Date] = [:]
    
    static func timerStart(_ name: String) {
        timers[name] = Date()
    }
    
    // return milliseconds
    static func getTimerElapsed(_ name: String) -> Double {
        guard let start = timers[name] else { return Double.nan }
        let elapsed = floor(start.timeIntervalSinceNow * -1000)
        return elapsed
    }
    
    static func timerPrint(_ name: String) {
        let ms: Double = getTimerElapsed(name)
        let time: String = ms.format(".0")
        Swift.print("\(name) in \(time) ms")
    }
}
