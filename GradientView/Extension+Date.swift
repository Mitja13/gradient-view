//
//  Date.swift
//  GradientView
//
//  Created by Mitja Semolic on 19/06/2018.
//  Copyright © 2018 Mitja Semolic. All rights reserved.
//

import Foundation

extension Date {
    init(iso: String) {
        self = Formatter.iso8601.date(from: iso)!
    }
    
    init(local: String) {
        self = Formatter.local.date(from: local)!
    }
    
    init(localDate: String, localTime: String) {
        let local = "\(localDate) \(localTime)"
        self.init(local: local)
    }
    
    var iso: String {
        return Formatter.iso8601.string(from: self)
    }
    
    var local: String {
        return Formatter.local.string(from: self)
    }
}

extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
    
    static let local: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
}

extension Double {
    func format(_ f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}
