//
//  NowLabel.swift
//  GradientView
//
//  Created by Mitja Semolic on 01/07/2018.
//  Copyright © 2018 Mitja Semolic. All rights reserved.
//

import UIKit

class NowLabel: UIView {
    @IBOutlet weak var timeLabel: UILabel!
    
    var contentView: UIView!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Setup view from .xib file
        setupNib()
    }
    
    func setNow(_ now: Date) {
        timeLabel.text = now.local
    }
    
    //DEL: func setFrame(positionY: CGFloat, convert: ViewController.Convert) {
    func setFrame(_ now: Date, convert: ViewController.Convert) {
        let positionY = convert.toPositionY(now)
        //let midY = positionY + convert.originY
        let newFrame = CGRect(midX: frame.midX, midY: positionY, width: frame.width, height: frame.height)
        //self.frame = newFrame
        //TODO: print("frame \(self.frame) \(newFrame)")
    }
    
    func setupNib() {
        contentView = loadNib()
        contentView.frame = bounds
        addSubview(contentView)
        
        contentView.translatesAutoresizingMaskIntoConstraints = true
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),
            contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
    }
    
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let view = bundle.loadNibNamed(nibName, owner: self, options: nil)!.first as! UIView
        return view
    }
}
