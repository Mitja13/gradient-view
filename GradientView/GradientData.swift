//
//  GradientData.swift
//  GradientView
//
//  Created by Mitja Semolic on 19/06/2018.
//  Copyright © 2018 Mitja Semolic. All rights reserved.
//

import Foundation

struct Daylight {
    var midnight0: Date
    var dawn: Date
    var sunrise: Date
    var daybreak: Date
    var noon: Date
    var nightfall: Date
    var sunset: Date
    var dusk: Date
    var midnight1: Date
    
    init(midnight0: Date, dawn: Date, sunrise: Date, daybreak: Date, noon: Date, nightfall: Date, sunset: Date, dusk: Date, midnight1: Date) {
        self.midnight0 = midnight0
        self.dawn = dawn
        self.sunrise = sunrise
        self.daybreak = daybreak
        self.noon = noon
        self.nightfall = nightfall
        self.sunset = sunset
        self.dusk = dusk
        self.midnight1 = midnight1
    }
}

/*
 - 11.57PM / midnight
 - 05.51AM / nautical dawn
 - 06.47AM / sunrise
 - 07.45AM / daybreak
 - 11.54AM / noon
 - 04.03PM / nightfall
 - 05.01PM / sunset
 - 05.57PM / nautical dusk
 - 11.58PM / midnight
 */

var DAYLIGHT1 = Daylight(
    midnight0: Date(local: "2018-06-18 23:57:00"),
    dawn: Date(local: "2018-06-19 05:51:00"),
    sunrise: Date(local: "2018-06-19 06:47:00"),
    daybreak: Date(local: "2018-06-19 07:45:00"),
    noon: Date(local: "2018-06-19 11:54:00"),
    nightfall: Date(local: "2018-06-19 16:03:00"),
    sunset: Date(local: "2018-06-19 17:01:00"),
    dusk: Date(local: "2018-06-19 17:57:00"),
    midnight1: Date(local: "2018-06-19 11:58:00"))
