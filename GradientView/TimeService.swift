//
//  TimeService.swift
//  GradientView
//
//  Created by Mitja Semolic on 30/06/2018.
//  Copyright © 2018 Mitja Semolic. All rights reserved.
//

import Foundation

var SERVICES = Services()

class Services {
    var timeService = TimeService()
    
    func start() {
        
        // bind to DATA here (for now consider to move)
        timeService.start(bindTime: DATA.time)
    }
}

class TimeService {
    private var bindTime: TimeObservable? = nil
    
    func start(bindTime: TimeObservable) {
        self.bindTime = bindTime
        
        // start a scheduled timer and catch updates in updateNow
        Timer.scheduledTimer(
            withTimeInterval: 0.5,
            repeats: true,
            block: updateNow)
    }
    
    private func updateNow(timer: Timer) {
        bindTime?.now = Date()
        //DEL: print(bindTime?.now.local ?? "nil")
    }
    
}
