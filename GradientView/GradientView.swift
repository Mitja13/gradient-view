//
//  GradientView.swift
//  GradientView
//
//  Created by Mitja Semolic on 14/06/2018.
//  Copyright © 2018 Mitja Semolic. All rights reserved.
//

import UIKit

@IBDesignable class GradientView: UIView {

    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        
        let colors: [CGColor] = [UIColor.white, UIColor.vividBlue, UIColor.mix(.vividBlue, .hotGreen), UIColor.hotGreen, UIColor.yellow, UIColor.orange, UIColor.red].map { $0.cgColor }
        let locations: [CGFloat] = [0, 1/6, 2/6, 3/6, 4/6, 5/6, 1]
        
        let gradient = CGGradient(
            colorsSpace: CGColorSpaceCreateDeviceRGB(),
            colors: colors as CFArray,
            locations: locations)!
        
        context?.drawLinearGradient(
            gradient,
            start: CGPoint.zero,
            end: CGPoint(x: 0, y: bounds.height),
            options: [])
    }
}
