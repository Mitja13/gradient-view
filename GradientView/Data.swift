//
//  Data.swift
//  GradientView
//
//  Created by Mitja Semolic on 30/06/2018.
//  Copyright © 2018 Mitja Semolic. All rights reserved.
//

import Foundation

let DATA = Data()

class Data {
    let time = TimeObservable()
}

class TimeObservable: NSObject {
    
    //NOTE: KVO-enabled properties must be @objc dynamic
    
    @objc dynamic var now = Date()
    //var alerts = [Alert]()
    
    //TODO: addAlert removeAlert
}

