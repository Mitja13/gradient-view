//
//  ViewController.swift
//  GradientView
//
//  Created by Mitja Semolic on 14/06/2018.
//  Copyright © 2018 Mitja Semolic. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    class Convert {
        var originTime = Date()
        var originY = CGFloat.nan
        var dayPixels = CGFloat.nan
        
        func toPositionY(_ fromTime: Date) -> CGFloat {
            let hourPixels = dayPixels / 24
            let hoursOffset = fromTime.timeIntervalSince(originTime) / (60 * 60)
            let positionY = CGFloat(hoursOffset) * hourPixels
            return positionY
        }
        
        func toTime(_ fromPositionY: CGFloat) -> Date {
            let hourPixels = dayPixels / 24
            let hoursOffset =  Double(fromPositionY / hourPixels)
            let time = originTime.addingTimeInterval(TimeInterval(hoursOffset * 60 * 60))
            return time
        }
        
        func getHorizonTime() -> Date {
            //let sectionY = view.convert(CGPoint(x: 0, y: horizonLine.center.y), to: atSection).y
            //let position = atSection.convertToPosition(sectionY: sectionY)
            //let time = convertToTime(position: position)
            //return time
            return Date()
        }
        
        func setup(initialTime: Date) {
            self.originTime = initialTime.addingTimeInterval(TimeInterval(-12 * 60 * 60))
            //self.originY = contentView.frame.midY
            self.originY = 0
            self.dayPixels = 2 * UIScreen.main.bounds.height
        }
    }
    
    private var observations = [NSKeyValueObservation]()
    private var convert = Convert()
    
    private var nowLabel: NowLabel?

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DEBUG.timerPrint("LOAD")
        
        // main viewDidLoad
        DEBUG.timerStart("SERVICES")
        SERVICES.start()
        DEBUG.timerPrint("SERVICES")
        
        // observe now
        let observeNow = DATA.time.observe(
            \.now,
            options: [.initial, .old],
            changeHandler: observedNow)
        observations += [observeNow]

    }
    
    private func observedNow(time: TimeObservable, change: NSKeyValueObservedChange<Date>) {
        let timeNow: Date = time.now
        //print("observed \(timeNow.local)")
        
        if change.oldValue == nil { convert.setup(initialTime: timeNow) }
        updateNowView(now: timeNow)
    }
    
    //TODO: func test
    func testScrollViewConversion() {
        //TODO: let markLabel = MarkLabel()
    }
    
    func updateNowView(now: Date) {
        
        // nowLabel
        if nowLabel == nil {
            nowLabel = NowLabel()
            nowLabel!.frame = CGRect(midX: UIScreen.main.bounds.width/2, midY: 400, width: 150, height: 150)
            contentView.addSubview(nowLabel!)
        }
        
        nowLabel!.setNow(now)
        nowLabel!.setFrame(now, convert: convert)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // hide status bar
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.isHidden = true
        
        // scroll to center
        print(scrollView.contentOffset)
        print(scrollView.contentInset)
        print(scrollView.contentSize)
        print(contentView.bounds)
        print(UIScreen.main.bounds)
        
        //let centerOffsetY = contentView.bounds.height / 2
        //scrollView.setContentOffset(CGPoint(x: 0, y: 400), animated: true)
        contentView.centerXAnchor.constraint(equalTo: scrollView.contentLayoutGuide.centerXAnchor)
        // Do the same for Y axis
        contentView.centerYAnchor.constraint(equalTo: scrollView.contentLayoutGuide.centerYAnchor)
    }
}

