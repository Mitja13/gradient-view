//
//  UIColor.swift
//  GradientView
//
//  Created by Mitja Semolic on 19/06/2018.
//  Copyright © 2018 Mitja Semolic. All rights reserved.
//

import UIKit

extension UIColor {
    public class var orange: UIColor { return UIColor(hex: 0xFC6621) }
    public class var sunshineYellow: UIColor { return UIColor(hex: 0xFCF035) }
    public class var hotGreen: UIColor { return UIColor(hex: 0x2BF931) }
    public class var vividBlue: UIColor { return UIColor(hex: 0x1024FA) }
    public class var indigo: UIColor { return UIColor(hex: 0x3F0D80) }
    
    public class var night: UIColor { return UIColor(hex: 0x120D42) }
    public class var twilight: UIColor { return UIColor(hex: 0x3121B0) }
    public class var golden: UIColor { return UIColor(hex: 0xFFFFFF) }
    public class var day: UIColor { return UIColor(hex: 0x57B1FF) }
    
    convenience init(hex: Int, alpha: CGFloat = 1.0) {
        self.init(
            red: CGFloat((hex >> 16) & 0xFF) / 255,
            green: CGFloat((hex >> 8) & 0xFF) / 255,
            blue: CGFloat(hex & 0xFF) / 255,
            alpha: alpha
        )
    }
    
    class func mix(_ color1: UIColor, _ color2: UIColor) -> UIColor {
        var (r1, g1, b1, a1) = (CGFloat(0), CGFloat(0), CGFloat(0), CGFloat(0))
        var (r2, g2, b2, a2) = (CGFloat(0), CGFloat(0), CGFloat(0), CGFloat(0))
        
        color1.getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
        color2.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)
        return UIColor(
            red: min(r1 + r2, 1),
            green: min(g1 + g2, 1),
            blue: min(b1 + b2, 1),
            alpha: (a1 + a2) / 2)
    }
}
