//
//  Extension+CGRect.swift
//  GradientView
//
//  Created by Mitja Semolic on 01/07/2018.
//  Copyright © 2018 Mitja Semolic. All rights reserved.
//

import UIKit

extension CGRect {
    init(midX: CGFloat, midY: CGFloat, width: CGFloat, height: CGFloat) {
        self.init(x: midX - width/2, y: midY - height/2, width: width, height: height)
    }
}
